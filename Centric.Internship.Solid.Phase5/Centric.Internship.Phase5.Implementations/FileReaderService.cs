﻿namespace Centric.Internship.Phase5.Implementations
{
    using System.Collections.Generic;
    using System.IO;

    using Centric.Internship.Phase5.Interfaces;
    using Centric.Internship.Phase5.Models;

    public class FileReaderService : IReaderService
    {
        private string _location;

        private IList<IContentParser> _contentParsers;

        public FileReaderService()
        {
            this._contentParsers = new List<IContentParser>();
        }

        public void RegisterFileReader(IContentParser parser)
        {
            this._contentParsers.Add(parser);
        }

        public Employees Read()
        {
            Employees employees = null;

            string input = File.ReadAllText(this._location);

            foreach (var fileReader in this._contentParsers)
            {
                if (fileReader.CanProcess(input))
                {
                    employees = fileReader.Process(input);
                    break;
                }
            }

            return employees;
        }

        public void SetLocation(string location)
        {
            this._location = location;
        }
    }
}
