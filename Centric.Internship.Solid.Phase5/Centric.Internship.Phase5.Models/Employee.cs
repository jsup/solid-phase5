﻿namespace Centric.Internship.Phase5.Models
{
    using System;
    using System.Xml.Serialization;

    [Serializable()]   
    public class Employee
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }

        [XmlElement(ElementName = "name")]
        public string Name { get; set; }

        [XmlArray(ElementName = "salaries")]
        public Salary[] Salaries { get; set; }
    }
}   