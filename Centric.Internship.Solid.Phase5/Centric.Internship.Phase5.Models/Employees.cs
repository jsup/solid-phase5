﻿namespace Centric.Internship.Phase5.Models
{
    using System.Xml.Serialization;

    [XmlRoot("Employees")]
    public class Employees
    {
        [XmlElement("Employee")]
        public Employee[] EmployeesCollection { get; set; }
    }
}