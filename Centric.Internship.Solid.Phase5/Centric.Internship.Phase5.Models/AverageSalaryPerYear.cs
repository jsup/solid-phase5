﻿namespace Centric.Internship.Phase5.Models
{
    using System;
    using System.Xml.Serialization;

    [Serializable()]
    public class AverageSalaryPerYear
    {
        [XmlElement(ElementName = "Year")]
        public string Year { get; set; }

        [XmlElement(ElementName = "AverageSalary")]
        public double AverageSalary { get; set; }
    }
}