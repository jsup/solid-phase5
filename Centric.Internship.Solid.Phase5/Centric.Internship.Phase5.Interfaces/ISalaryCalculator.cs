﻿namespace Centric.Internship.Phase5.Interfaces
{
    using System;

    using Centric.Internship.Phase5.Models;

    public interface ISalaryCalculator
    {
        void CalculateAverageSalary();

        void CalculateAverageSalaryForEmployees(Func<Employees, Employees> filter);
    }
}