﻿namespace Centric.Internship.Phase5.Interfaces
{
    using Centric.Internship.Phase5.Models;

    public interface IReaderService
    {
        void SetLocation(string location);

        void RegisterFileReader(IContentParser parser);
       
        Employees Read();
    }
}
