﻿namespace Centric.Internship.Phase5.Tests
{
    using Centric.Internship.Phase5.App;
    using Centric.Internship.Phase5.Implementations;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class CalculateSalaryPerYear
    {   
        [TestMethod]
        public void CalculateSalaryFromFileWithFilter()
        {
            var fileReaderService = new FileReaderService();

            var outputFileManager = new XmlOutputFileManager();

            fileReaderService.RegisterFileReader(new JSONContentParser());

            fileReaderService.SetLocation(@"..\..\..\..\SalariesPerYear.json");

            var app = new SalaryApp();

            app.Run(fileReaderService, outputFileManager);
        }
    }
}